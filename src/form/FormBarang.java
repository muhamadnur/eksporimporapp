/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import management.*;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import koneksi.MyConnection;

/**
 *
 * @author Admin
 */
public class FormBarang extends javax.swing.JFrame {

    PreparedStatement ps;
    ResultSet rs;
    String nama = null;
    String jenis = null;
    String jumlah = null;
    String id = null;
    String merek = null;
    String tipe = null;
    String ukuran = null;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Creates new form FormPenjualPenerima
     */
    public FormBarang() {
        initComponents();
        autonumber();
        this.setLocationRelativeTo(null);
        txt_id.setEditable(false);
        txt_nama.requestFocus();
        kosong();
    }

    private void autonumber() {
        try {
            String query = "select MAX(RIGHT(id,8)) AS NO  from barang order by id desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    txt_id.setText("IBR00000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    txt_id.setText("IBR" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    private void kosong() {
        txt_nama.setText("");
        txt_jumlah.setText("");
        txt_merek.setText("");
        txt_jenis.setText("");
        txt_tipe.setText("");
        txt_ukuran.setText("");
    }

    private void saveData() {
        id = txt_id.getText();
        nama = txt_nama.getText().trim();
        merek = txt_merek.getText().trim();
        jumlah = txt_jumlah.getText().trim();
        jenis = txt_jenis.getText().trim();
        tipe = txt_tipe.getText().trim();
        ukuran = txt_ukuran.getText().trim();
        if (nama.isEmpty()) {
            showMessage("Name cannot be empty!");
            txt_nama.requestFocus();
        } else if (jenis.isEmpty()) {
            showMessage("Jenis cannot be empty!");
            txt_jenis.requestFocus();
        } else if (jumlah.isEmpty()) {
            showMessage("Jumlah cannot be empty!");
            txt_jumlah.requestFocus();
        } else if (merek.isEmpty()) {
            showMessage("Merek cannot be empty!");
            txt_merek.requestFocus();
        } else if (tipe.isEmpty()) {
            showMessage("Tipe cannot be empty!");
            txt_tipe.requestFocus();
        } else if (ukuran.isEmpty()) {
            showMessage("Ukuran cannot be empty!");
            txt_ukuran.requestFocus();
        } else {
            String query = "INSERT INTO `barang`(`id`, `nama`, `jenis`, `jumlah`, `merek`, `tipe`, `ukuran`) VALUES(?,?,?,?,?,?,?)";
            try {
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.setString(1, id);
                ps.setString(2, nama);
                ps.setString(3, jenis);
                ps.setString(4, jumlah);
                ps.setString(5, merek);
                ps.setString(6, tipe);
                ps.setString(7, ukuran);

                ps.executeUpdate();
                showMessage("Data saved successfully!");
                kosong();
                autonumber();
                this.dispose();
            } catch (Exception e) {
                showMessage("Data failed to save " + e);
            }
        }
    }

    private void editData() {
        id = txt_id.getText();
        nama = txt_nama.getText().trim();
        merek = txt_merek.getText().trim();
        jumlah = txt_jumlah.getText().trim();
        jenis = txt_jenis.getText().trim();
        tipe = txt_tipe.getText().trim();
        ukuran = txt_ukuran.getText().trim();
        if (nama.isEmpty()) {
            showMessage("Name cannot be empty!");
            txt_nama.requestFocus();
        } else if (jenis.isEmpty()) {
            showMessage("Jenis cannot be empty!");
            txt_jenis.requestFocus();
        } else if (jumlah.isEmpty()) {
            showMessage("Jumlah cannot be empty!");
            txt_jumlah.requestFocus();
        } else if (merek.isEmpty()) {
            showMessage("Merek cannot be empty!");
            txt_merek.requestFocus();
        } else if (tipe.isEmpty()) {
            showMessage("Tipe cannot be empty!");
            txt_tipe.requestFocus();
        } else if (ukuran.isEmpty()) {
            showMessage("Ukuran cannot be empty!");
            txt_ukuran.requestFocus();
        } else {
            String query = "UPDATE\n"
                    + "    `barang`\n"
                    + "SET\n"
                    + "    `nama` = ?,\n"
                    + "    `jenis` = ?,\n"
                    + "    `jumlah` = ?,\n"
                    + "    `merek` = ?,\n"
                    + "    `tipe` = ?,\n"
                    + "    `ukuran` = ?\n"
                    + "WHERE id='" + id + "'";
            try {
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.setString(1, nama);
                ps.setString(2, jenis);
                ps.setString(3, jumlah);
                ps.setString(4, merek);
                ps.setString(5, tipe);
                ps.setString(6, ukuran);
                ps.executeUpdate();
                showMessage("Data successfully changed!");
                kosong();
                this.dispose();

            } catch (Exception e) {
                showMessage("Data failed to change! " + e);
            }
        }
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_id = new javax.swing.JTextField();
        txt_nama = new javax.swing.JTextField();
        txt_jumlah = new javax.swing.JTextField();
        btn_save = new javax.swing.JButton();
        btn_edit = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txt_jenis = new javax.swing.JTextField();
        txt_merek = new javax.swing.JTextField();
        txt_tipe = new javax.swing.JTextField();
        txt_ukuran = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Barang");

        jLabel2.setText("ID");

        jLabel3.setText("Nama");

        jLabel4.setText("Jumlah");

        txt_nama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_namaKeyPressed(evt);
            }
        });

        txt_jumlah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_jumlahKeyPressed(evt);
            }
        });

        btn_save.setBackground(new java.awt.Color(0, 102, 204));
        btn_save.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_save.setForeground(new java.awt.Color(255, 255, 255));
        btn_save.setText("Save");
        btn_save.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        btn_edit.setBackground(new java.awt.Color(0, 102, 204));
        btn_edit.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_edit.setForeground(new java.awt.Color(255, 255, 255));
        btn_edit.setText("Edit");
        btn_edit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editActionPerformed(evt);
            }
        });

        btn_cancel.setBackground(new java.awt.Color(255, 102, 102));
        btn_cancel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cancel.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancel.setText("Cancel");
        btn_cancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        jLabel6.setText("Tipe");

        jLabel7.setText("Merek");

        jLabel5.setText("Jenis");

        txt_jenis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_jenisKeyPressed(evt);
            }
        });

        txt_merek.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_merekKeyPressed(evt);
            }
        });

        txt_tipe.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_tipeKeyPressed(evt);
            }
        });

        txt_ukuran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_ukuranKeyPressed(evt);
            }
        });

        jLabel8.setText("Ukuran");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btn_cancel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_edit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_save))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(10, 10, 10)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_id, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txt_jumlah, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txt_jenis)
                                    .addComponent(txt_nama, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                                    .addComponent(txt_merek)
                                    .addComponent(txt_tipe)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(txt_ukuran, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txt_jenis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt_jumlah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txt_merek, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txt_tipe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_ukuran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_save)
                    .addComponent(btn_edit)
                    .addComponent(btn_cancel))
                .addGap(30, 30, 30))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        // TODO add your handling code here:
        saveData();
    }//GEN-LAST:event_btn_saveActionPerformed

    private void txt_namaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_namaKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            nama = txt_nama.getText();
            if (nama.isEmpty()) {
                showMessage("Name cannot be empty!");
                txt_nama.requestFocus();
            } else {
                txt_jenis.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_namaKeyPressed

    private void txt_jumlahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_jumlahKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jumlah = txt_jumlah.getText();
            if (jumlah.isEmpty()) {
                showMessage("Jumlah cannot be empty!");
                txt_jumlah.requestFocus();
            } else {
                txt_merek.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_jumlahKeyPressed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        // TODO add your handling code here:
        kosong();
        this.dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editActionPerformed
        // TODO add your handling code here:
        editData();
    }//GEN-LAST:event_btn_editActionPerformed

    private void txt_jenisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_jenisKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jenis = txt_jenis.getText();
            if (jenis.isEmpty()) {
                showMessage("Jenis cannot be empty!");
                txt_jenis.requestFocus();
            } else {
                txt_jumlah.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_jenisKeyPressed

    private void txt_merekKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_merekKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            merek = txt_merek.getText();
            if (merek.isEmpty()) {
                showMessage("Merek cannot be empty!");
                txt_merek.requestFocus();
            } else {
                txt_tipe.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_merekKeyPressed

    private void txt_tipeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_tipeKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            tipe = txt_tipe.getText();
            if (tipe.isEmpty()) {
                showMessage("Tipe cannot be empty!");
                txt_tipe.requestFocus();
            } else {
                txt_ukuran.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_tipeKeyPressed

    private void txt_ukuranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_ukuranKeyPressed
        // TODO add your handling code here:
         if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            ukuran = txt_ukuran.getText();
            if (ukuran.isEmpty()) {
                showMessage("Ukuran cannot be empty!");
                txt_ukuran.requestFocus();
            } else {
                txt_ukuran.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_ukuranKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormBarang.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormBarang.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormBarang.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormBarang.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormBarang().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    public javax.swing.JButton btn_edit;
    public javax.swing.JButton btn_save;
    public javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTextField txt_id;
    public javax.swing.JTextField txt_jenis;
    public javax.swing.JTextField txt_jumlah;
    public javax.swing.JTextField txt_merek;
    public javax.swing.JTextField txt_nama;
    public javax.swing.JTextField txt_tipe;
    public javax.swing.JTextField txt_ukuran;
    // End of variables declaration//GEN-END:variables
}
