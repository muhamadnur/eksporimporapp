/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import management.*;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import koneksi.MyConnection;

/**
 *
 * @author Admin
 */
public class FormPPJK extends javax.swing.JFrame {

    PreparedStatement ps;
    ResultSet rs;
    String status = null;
    String nama = null;
    String nomor = null;
    String alamat = null;
    String npwp = null;
    String id = null;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Creates new form FormPenjualPenerima
     */
    public FormPPJK() {
        initComponents();
        autonumber();
        this.setLocationRelativeTo(null);
        txt_id.setEditable(false);
        txt_nama.requestFocus();
        kosong();
    }

    private void autonumber() {
        try {
            String query = "select MAX(RIGHT(id,8)) AS NO  from ppjk order by id desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    txt_id.setText("IPP00000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    txt_id.setText("IPP" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    private void kosong() {
        txt_nama.setText("");
        txt_alamat.setText("");
        txt_nomor.setText("");
        txt_npwp.setText("");
    }

    private void saveData() {
        id = txt_id.getText();
        nama = txt_nama.getText().trim();
        nomor = txt_nomor.getText().trim();
        alamat = txt_alamat.getText().trim();
        npwp = txt_npwp.getText().trim();
        String tanggal = String.valueOf(dateFormat.format(txt_tanggal.getDate()));
        if (nama.isEmpty()) {
            showMessage("Name cannot be empty!");
            txt_nama.requestFocus();
        } else if (npwp.isEmpty()) {
            showMessage("NPWP cannot be empty!");
            txt_npwp.requestFocus();
        } else if (alamat.isEmpty()) {
            showMessage("Alamat cannot be empty!");
            txt_alamat.requestFocus();
        } else if (nomor.isEmpty()) {
            showMessage("Negara cannot be empty!");
            txt_nomor.requestFocus();
        } else {
            String query = "INSERT INTO `ppjk`(`id`, `nama`, `npwp`, `alamat`, `nomor`, `tgl_ppjk`) VALUES (?,?,?,?,?,?)";
            try {
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.setString(1, id);
                ps.setString(2, nama);
                ps.setString(3, npwp);
                ps.setString(4, alamat);
                ps.setString(5, nomor);
                ps.setString(6, tanggal);

                ps.executeUpdate();
                showMessage("Data saved successfully!");
                kosong();
                autonumber();
                this.dispose();
            } catch (Exception e) {
                showMessage("Data failed to save " + e);
            }
        }
    }

    private void editData() {
        id = txt_id.getText();
        nama = txt_nama.getText().trim();
        npwp = txt_npwp.getText().trim();
        nomor = txt_nomor.getText().trim();
        alamat = txt_alamat.getText().trim();
        Date tanggal = txt_tanggal.getDate();
        if (nama.isEmpty()) {
            showMessage("Name cannot be empty!");
            txt_nama.requestFocus();
        } else if (npwp.isEmpty()) {
            showMessage("NPWP cannot be empty!");
            txt_npwp.requestFocus();
        } else if (alamat.isEmpty()) {
            showMessage("Alamat cannot be empty!");
            txt_alamat.requestFocus();
        } else if (nomor.isEmpty()) {
            showMessage("Negara cannot be empty!");
            txt_nomor.requestFocus();
        } else {
            String query = "UPDATE\n"
                    + "    `ppjk`\n"
                    + "SET\n"
                    + "    `nama` = ?,\n"
                    + "    `npwp` = ?,\n"
                    + "    `alamat` = ?,\n"
                    + "    `nomor` = ?,\n"
                    + "    `tgl_ppjk` = ?\n"
                    + "WHERE id='" + id + "'";
            try {
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.setString(1, nama);
                ps.setString(2, npwp);
                ps.setString(3, alamat);
                ps.setString(4, nomor);
                ps.setString(5, tanggal.toString());
                ps.executeUpdate();
                showMessage("Data successfully changed!");
                kosong();
                this.dispose();

            } catch (Exception e) {
                showMessage("Data failed to change! " + e);
            }
        }
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_id = new javax.swing.JTextField();
        txt_nama = new javax.swing.JTextField();
        txt_alamat = new javax.swing.JTextField();
        btn_save = new javax.swing.JButton();
        btn_edit = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txt_tanggal = new com.toedter.calendar.JDateChooser();
        txt_nomor = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txt_npwp = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("PPJK");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 305, 28));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 28, 338, 10));

        jLabel2.setText("ID");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 51, 68, -1));

        jLabel3.setText("Nama");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 79, 68, -1));

        jLabel4.setText("Alamat");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 135, 68, -1));
        jPanel1.add(txt_id, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 48, 230, -1));

        txt_nama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_namaKeyPressed(evt);
            }
        });
        jPanel1.add(txt_nama, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 76, 230, -1));

        txt_alamat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_alamatKeyPressed(evt);
            }
        });
        jPanel1.add(txt_alamat, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 132, 230, -1));

        btn_save.setBackground(new java.awt.Color(0, 102, 204));
        btn_save.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_save.setForeground(new java.awt.Color(255, 255, 255));
        btn_save.setText("Save");
        btn_save.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });
        jPanel1.add(btn_save, new org.netbeans.lib.awtextra.AbsoluteConstraints(257, 233, -1, -1));

        btn_edit.setBackground(new java.awt.Color(0, 102, 204));
        btn_edit.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_edit.setForeground(new java.awt.Color(255, 255, 255));
        btn_edit.setText("Edit");
        btn_edit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editActionPerformed(evt);
            }
        });
        jPanel1.add(btn_edit, new org.netbeans.lib.awtextra.AbsoluteConstraints(194, 233, -1, -1));

        btn_cancel.setBackground(new java.awt.Color(255, 102, 102));
        btn_cancel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cancel.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancel.setText("Cancel");
        btn_cancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });
        jPanel1.add(btn_cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(117, 233, -1, -1));

        jLabel6.setText("Tanggal");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 188, 68, -1));
        jPanel1.add(txt_tanggal, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 188, 230, -1));
        jPanel1.add(txt_nomor, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 160, 230, -1));

        jLabel7.setText("Nomor PPJK");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 163, -1, -1));

        jLabel5.setText("NPWP");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 107, 68, -1));

        txt_npwp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_npwpKeyPressed(evt);
            }
        });
        jPanel1.add(txt_npwp, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 104, 230, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        // TODO add your handling code here:
        saveData();
    }//GEN-LAST:event_btn_saveActionPerformed

    private void txt_namaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_namaKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            nama = txt_nama.getText();
            if (nama.isEmpty()) {
                showMessage("Name cannot be empty!");
                txt_nama.requestFocus();
            } else {
                txt_npwp.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_namaKeyPressed

    private void txt_alamatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_alamatKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            alamat = txt_alamat.getText();
            if (alamat.isEmpty()) {
                showMessage("Alamat cannot be empty!");
                txt_alamat.requestFocus();
            } else {
                txt_nomor.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_alamatKeyPressed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        // TODO add your handling code here:
        kosong();
        this.dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editActionPerformed
        // TODO add your handling code here:
        editData();
    }//GEN-LAST:event_btn_editActionPerformed

    private void txt_npwpKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_npwpKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            npwp = txt_npwp.getText();
            if (npwp.isEmpty()) {
                showMessage("NPWP cannot be empty!");
                txt_npwp.requestFocus();
            } else {
                txt_alamat.requestFocus();
            }
        }
    }//GEN-LAST:event_txt_npwpKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormPPJK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormPPJK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormPPJK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormPPJK.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormPPJK().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    public javax.swing.JButton btn_edit;
    public javax.swing.JButton btn_save;
    public javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    public javax.swing.JTextField txt_alamat;
    public javax.swing.JTextField txt_id;
    public javax.swing.JTextField txt_nama;
    public javax.swing.JTextField txt_nomor;
    public javax.swing.JTextField txt_npwp;
    public com.toedter.calendar.JDateChooser txt_tanggal;
    // End of variables declaration//GEN-END:variables
}
