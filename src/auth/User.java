/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth;

/**
 *
 * @author Admin
 */
public class User {
    private static String id_user;
    private static String name;
    private static int role_id;
    private static String username;
    private static int is_active;

    public User() {
    }

    public static String getId_user() {
        return id_user;
    }

    public static void setId_user(String id_user) {
        User.id_user = id_user;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        User.name = name;
    }

    public static int getRole_id() {
        return role_id;
    }

    public static void setRole_id(int role_id) {
        User.role_id = role_id;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        User.username = username;
    }

    public static int getIs_active() {
        return is_active;
    }

    public static void setIs_active(int is_active) {
        User.is_active = is_active;
    }
    
}
