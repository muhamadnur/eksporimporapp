/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.User;
import auth.FormChangePassword;
import auth.LoginForm;
import form.FormReport;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import koneksi.MyConnection;
import management.DataBarang;
import management.DataKantoPabaean;
import management.DataPenjualPanel;
import management.DataEksportirPanel;
import management.DataImportirPanel;
import management.DataPPJK;
import management.DataPemilikBarang;
import menu.MenuItem;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import transaksi.TransaksiEksporPanel;
import transaksi.TransaksiImporPanel;

/**
 *
 * @author Admin
 */
public class Main extends javax.swing.JFrame {

    PreparedStatement ps;
    ResultSet rs;
    String id_user = User.getId_user();
    String name = User.getName();
    String username = User.getUsername();
    int role_id = User.getRole_id();
    JpanelLoader jpload = new JpanelLoader();

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        HomePanel home = new HomePanel();
        jpload.jPanelLoader(panelBody, home);
        label_name.setText(name);
        execute();
    }

    private void execute() {
        String penjual = "Data Pemasok";
        String home = "Home";
        String t_import = "Transaction Impor";
        String t_ekspor = "Transaction Ekspor";
        String kantor = "Data Kantor Pabean";
        String eksportir = "Data Eksportir";
        String importir = "Data Importir";
        String barang = "Data Barang";
        String pemilik = "Data Pemilik Barang";
        ImageIcon iconHome = new ImageIcon(getClass().getResource("/icon/icon_home.png"));
        ImageIcon icontransaction = new ImageIcon(getClass().getResource("/icon/transaction.png"));
        ImageIcon iconreport = new ImageIcon(getClass().getResource("/icon/report.png"));
        ImageIcon iconDatabase = new ImageIcon(getClass().getResource("/icon/database.png"));
        ImageIcon iconSubMenu = new ImageIcon(getClass().getResource("/icon/subMenu.png"));
        ImageIcon iconNext = new ImageIcon(getClass().getResource("/icon/next.png"));
        ImageIcon iconLogout = new ImageIcon(getClass().getResource("/icon/logout.png"));
        ImageIcon iconProfile = new ImageIcon(getClass().getResource("/icon/user.png"));
        ImageIcon iconKey = new ImageIcon(getClass().getResource("/icon/icon_key.png"));

        //menu home
        MenuItem menuHome = new MenuItem(iconHome, home, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HomePanel home = new HomePanel();
                jpload.jPanelLoader(panelBody, home);
            }
        });
        //transaksi impor
        MenuItem impor = new MenuItem(icontransaction, t_import, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TransaksiImporPanel pro = new TransaksiImporPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem ekspor = new MenuItem(icontransaction, t_ekspor, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TransaksiEksporPanel pro = new TransaksiEksporPanel();
                jpload.jPanelLoader(panelBody, pro);
//                FormTransaksiEkspor eks = new FormTransaksiEkspor();
//                eks.setVisible(true);
//                eks.pack();
//                eks.setLocationRelativeTo(null);
//                eks.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });
        //menu management
        //data kantor pabean
        MenuItem dataKantor = new MenuItem(iconSubMenu, kantor, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataKantoPabaean pro = new DataKantoPabaean();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //eksportir
        MenuItem dataEksportir = new MenuItem(iconSubMenu, eksportir, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataEksportirPanel pro = new DataEksportirPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //importir
        MenuItem dataImportir = new MenuItem(iconSubMenu, importir, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataImportirPanel pro = new DataImportirPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //Penjual
        MenuItem dataPenjual = new MenuItem(iconSubMenu, penjual, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataPenjualPanel pro = new DataPenjualPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //barang
        MenuItem dataBarang = new MenuItem(iconSubMenu, barang, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataBarang pro = new DataBarang();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //pemilik barang
        MenuItem pemilikBarang = new MenuItem(iconSubMenu, pemilik, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataPemilikBarang pro = new DataPemilikBarang();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //ppjk
        MenuItem ppjk = new MenuItem(iconSubMenu, "Data PPJK", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataPPJK pro = new DataPPJK();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuDatabase = new MenuItem(iconDatabase, "Management", null, dataBarang, pemilikBarang, dataKantor, dataEksportir, dataImportir, dataPenjual, ppjk);
        MenuItem changePassword = new MenuItem(iconKey, "Change Password", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FormChangePassword pass = new FormChangePassword();
                pass.setVisible(true);
                pass.pack();
                pass.setLocationRelativeTo(null);
                pass.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });
        //Report
        MenuItem reportDataKantor = new MenuItem(iconSubMenu, "Report Data Kantor Pabean", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataKantor();
            }
        });
        MenuItem imporItem = new MenuItem(iconNext, "Transaction Impor", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printRecapTransaksiImpor();
            }
        });
        MenuItem eksporItem = new MenuItem(iconNext, "Transaction Ekspor", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printRecapTransaksiEkspor();
            }
        });
        MenuItem recap = new MenuItem(iconSubMenu, "Recap Transaction", null, imporItem, eksporItem);
        //report daily
        MenuItem dailyImpor = new MenuItem(iconNext, "Daily Transaction Impor", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataDailyImpor();
            }
        });
        MenuItem dailyEkspor = new MenuItem(iconNext, "Daily Transaction Ekspor", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataDailyEkspor();
            }
        });
        MenuItem daily = new MenuItem(iconSubMenu, "Daily Report", null, dailyImpor, dailyEkspor);
        MenuItem eksportirItem = new MenuItem(iconSubMenu, "Report Data Eksportir", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataEksportir();
            }
        });
        MenuItem importirItem = new MenuItem(iconSubMenu, "Report Data Importir", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataImportir();
            }
        });
        MenuItem barangItem = new MenuItem(iconSubMenu, "Report Data Barang", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataBarang();
            }
        });
        MenuItem transaksiItem = new MenuItem(iconSubMenu, "Report Transaksi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FormReport pass = new FormReport();
                pass.setVisible(true);
                pass.pack();
                pass.setLocationRelativeTo(null);
                pass.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });
        MenuItem report = new MenuItem(iconreport, "Report", null, recap,transaksiItem, daily, reportDataKantor, eksportirItem, importirItem,barangItem);
        MenuItem menuLogout = new MenuItem(iconLogout, "Log Out", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ok = JOptionPane.showConfirmDialog(null, "Log Out?", "Message", 2, JOptionPane.YES_NO_OPTION);
                if (ok == 0) {
                    showMessage("Log out successfully!");
                    LoginForm login = new LoginForm();
                    login.setVisible(true);
                    login.pack();
                    login.setLocationRelativeTo(null);
                    login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    dispose();
                }
            }
        });
        addMenu(menuHome, impor, ekspor, menuDatabase, report, changePassword, menuLogout);
    }

    private void addMenu(MenuItem... menu) {
        for (int i = 0; i < menu.length; i++) {
            menus.add(menu[i]);
            ArrayList<MenuItem> subMenu = menu[i].getSubMenu();
            for (MenuItem m : subMenu) {
                addMenu(m);
            }
        }
        menus.revalidate();
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }
    private void printDataBarang() {
        try {
            String namaFile = "src\\report\\reportDataBarang.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    private void printDataDailyEkspor() {
        try {
            String namaFile = "src\\report\\reportDailyEkspor.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    private void printDataDailyImpor() {
        try {
            String namaFile = "src\\report\\reportDailyImpor.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printDataImportir() {
        try {
            String namaFile = "src\\report\\reportDataImportir.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printDataEksportir() {
        try {
            String namaFile = "src\\report\\reportDataEksportir.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printDataKantor() {
        try {
            String namaFile = "src\\report\\reportDataKantor.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printRecapTransaksiImpor() {
        try {
            String namaFile = "src\\report\\reportRecapTransaksiImpor.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printRecapTransaksiEkspor() {
        try {
            String namaFile = "src\\report\\reportRecapTransaksiEkspor.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getContentPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelHeader = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        label_name = new javax.swing.JLabel();
        panelMenu = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        menus = new javax.swing.JPanel();
        panelBody = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelHeader.setBackground(new java.awt.Color(0, 204, 204));
        panelHeader.setPreferredSize(new java.awt.Dimension(561, 50));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("PT. MATAHARI TERBIT EKPRESS");

        label_name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        label_name.setForeground(new java.awt.Color(255, 255, 255));
        label_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_name.setText("Nama");

        javax.swing.GroupLayout panelHeaderLayout = new javax.swing.GroupLayout(panelHeader);
        panelHeader.setLayout(panelHeaderLayout);
        panelHeaderLayout.setHorizontalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(312, 312, 312)
                .addComponent(label_name, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelHeaderLayout.setVerticalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(label_name, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(panelHeader, java.awt.BorderLayout.PAGE_START);

        panelMenu.setBackground(new java.awt.Color(0, 204, 204));
        panelMenu.setPreferredSize(new java.awt.Dimension(250, 384));

        jScrollPane1.setBorder(null);

        menus.setBackground(new java.awt.Color(255, 255, 255));
        menus.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        menus.setLayout(new javax.swing.BoxLayout(menus, javax.swing.BoxLayout.Y_AXIS));
        jScrollPane1.setViewportView(menus);

        javax.swing.GroupLayout panelMenuLayout = new javax.swing.GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        panelMenuLayout.setVerticalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
        );

        getContentPane().add(panelMenu, java.awt.BorderLayout.LINE_START);

        panelBody.setBackground(new java.awt.Color(255, 255, 255));
        panelBody.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout panelBodyLayout = new javax.swing.GroupLayout(panelBody);
        panelBody.setLayout(panelBodyLayout);
        panelBodyLayout.setHorizontalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 636, Short.MAX_VALUE)
        );
        panelBodyLayout.setVerticalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );

        getContentPane().add(panelBody, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(906, 496));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel label_name;
    private javax.swing.JPanel menus;
    public javax.swing.JPanel panelBody;
    private javax.swing.JPanel panelHeader;
    private javax.swing.JPanel panelMenu;
    // End of variables declaration//GEN-END:variables

}
