/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transaksi;

import auth.User;
import java.awt.Container;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import koneksi.MyConnection;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import table.TableDataBarang;
import table.TableDataEksportir;
import table.TableDataPPJK;
import table.TableKantorPabean;

/**
 *
 * @author Admin
 */
public class TransaksiEksporPanel extends javax.swing.JPanel {

    public String kode_kantor;
    public String id_eksportir;
    public String id_ppjk;
    public String id_barang;
    String id_user = User.getId_user();
    PreparedStatement ps;
    ResultSet rs;
    String id_Transaksi = "";
    String nomor = "";
    String c_perdagangan = null;
    String jenis = null;
    String kategori = null;
    String pembayaran = null;
    String pengangkutan = null;
    String penyerahan = null;
    Date perkiraan = null;
    Date t_invoice = null;
    Date t_dokumen = null;

    SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd");
    Date date = new Date();

    String tanggal = String.valueOf(formatDate.format(date));

    /**
     * Creates new form TransaksiEksporPanel
     */
    public TransaksiEksporPanel() {
        initComponents();
        autonumber();
        pengajuan();
        kosong();
        txt_nomorPeng.setEditable(false);
    }

    private void autonumber() {
        try {
            String query = "select MAX(RIGHT(id_transaksi,8)) AS NO  from peb order by id_transaksi desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    id_Transaksi = "IDT0000001";
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    id_Transaksi = "IDT" + no;
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    public void itemTerpilih() {
        TableKantorPabean Dpw = new TableKantorPabean();
        Dpw.ekspor = this;
        txt_kode.setText(kode_kantor);
        txt_nomorPeng.setText(kode_kantor + "-" + nomor + "-" + tanggal + "-" + nomor);
        TableDataEksportir eks = new TableDataEksportir();
        eks.ekspor = this;
        txt_idEksportir.setText(id_eksportir);
        TableDataEksportir ppjk = new TableDataEksportir();
        ppjk.ekspor = this;
        txt_idPPJK.setText(id_ppjk);
        TableDataBarang barang = new TableDataBarang();
        barang.ekspor = this;
        txt_idBarang.setText(id_barang);
    }

    private void pengajuan() {
        try {
            String query = "select MAX(RIGHT(id_transaksi,6)) AS NO  from peb order by id_transaksi desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    nomor = "000001";
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 6 - Nomor; j++) {
                        no = "0" + no;
                    }
                    nomor = no;
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }
    }

    private void saveDataPEB() {
        String kode = txt_kode.getText().trim();
        String pengajuan = txt_nomorPeng.getText().trim();
        String jenis = jComboJenis.getSelectedItem().toString();
        String kategori = jComboKategori.getSelectedItem().toString();
        if (jRadioButton3.isSelected()) {
            c_perdagangan = "Imbal Dagang";
        } else {
            c_perdagangan = "Lainnya";
        }
        String pembayaran = jComboPembayaran.getSelectedItem().toString();
        String eksportir = txt_idEksportir.getText().trim();
        String nama = txt_nama.getText().trim();
        String alamat = txt_alamat.getText().trim();
        String ppjk = txt_idPPJK.getText().trim();
        String pengangkutan = jComboPengangkutan.getSelectedItem().toString();
        String sarana = txt_sarana.getText().trim();
        String no_pengangkut = txt_pengangkut.getText().trim();
        String bendera = txt_bendera.getText().trim();
        String perkiraan = jDatePerkiraan.getDate().toString();
        String query = "INSERT INTO `peb`(`id_transaksi`, `kantor_id`, `nomor_pengajuan`, `jenis_ekspor`, `kategori_ekspor`, `cara_perdagangan`,"
                + " `cara_pembayaran`, `eksportir_id`, `nama_penerima`, `alamat_penerima`, `ppjk_id`, `cara_pengangkutan`, `nama_sarana_pengangkut`, "
                + "`nomor_pengangkut`, `bendera_sarana_pengangkut`, `tanggal_perkiraan_ekspor`, `user_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, id_Transaksi);
            ps.setString(2, kode);
            ps.setString(3, pengajuan);
            ps.setString(4, jenis);
            ps.setString(5, kategori);
            ps.setString(6, c_perdagangan);
            ps.setString(7, pembayaran);
            ps.setString(8, eksportir);
            ps.setString(9, nama);
            ps.setString(10, alamat);
            ps.setString(11, ppjk);
            ps.setString(12, pengangkutan);
            ps.setString(13, sarana);
            ps.setString(14, no_pengangkut);
            ps.setString(15, bendera);
            ps.setString(16, perkiraan);
            ps.setString(17, id_user);
            ps.executeUpdate();
        } catch (Exception e) {
            showMessage("" + e);
        }
    }

    private void saveDataPEBPelabuhan() {
        String asal = txt_muat_asal.getText().trim();
        String tempat_muat = txt_muat_ekspor.getText().trim();
        String transit = txt_transit.getText().trim();
        String bongkar = txt_bongkar.getText().trim();
        String lokasi = txt_lokasi.getText().trim();
        String kantor = txt_kantor.getText().trim();
        String invoice = txt_no_invoice.getText().trim();
        String t_invoice = jDateInvoice.getDate().toString();
        String jenis_dokumen = txt_jenis_dokumen.getText().trim();
        String no_dokumen = txt_no_dokumen.getText().trim();
        String t_dokumen = jDateDokumen.getDate().toString();
        String daerah = txt_daerah.getText().trim();
        String tujuan = txt_tujuan.getText().trim();
        String penyerahan = jComboPenyerahan.getSelectedItem().toString();
        String devisa = txt_devisa.getText().trim();
        String valuta = txt_valuta.getText().trim();
        String freight = txt_freight.getText().trim();
        String asuransi = txt_asuransi.getText().trim();
        String fob = txt_fob.getText().trim();
        String query = "INSERT INTO `peb_pelabuhan`( `pelabuhan_muat_asal`, `tempat_muat_ekspor`, `pelabuhan_transit`, `plabuhan_bongkar`,"
                + " `lokasi_pemerikasaan`, `kantor_pabean_pemerikasaan`, `nomor_invoice`, `tanggal_invoice`, `jenis_dokumen`, `nomor_dokumen`,"
                + " `tanggal_dokumen`, `daerah_asal_barang`, `negara_tujuan`, `cara_penyerahan_barang`, `bank_devisa`, `jenis_valuta`, `freight`, "
                + "`asuransi`, `fob`, `transaksi_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, asal);
            ps.setString(2, tempat_muat);
            ps.setString(3, transit);
            ps.setString(4, bongkar);
            ps.setString(5, lokasi);
            ps.setString(6, kantor);
            ps.setString(7, invoice);
            ps.setString(8, t_invoice);
            ps.setString(9, jenis_dokumen);
            ps.setString(10, no_dokumen);
            ps.setString(11, t_dokumen);
            ps.setString(12, daerah);
            ps.setString(13, tujuan);
            ps.setString(14, penyerahan);
            ps.setString(15, devisa);
            ps.setString(16, valuta);
            ps.setString(17, freight);
            ps.setString(18, asuransi);
            ps.setString(19, fob);
            ps.setString(20, id_Transaksi);
            ps.executeUpdate();
        } catch (Exception e) {
            showMessage("" + e);
        }
    }

    private void saveDataPEBPetikemas() {
        String petikemas = txt_petikemas.getText().trim();
        String status_petikemas = txt_status_petikemas.getText().trim();
        String jml_petikemas = txt_jumlah_petikemas.getText().trim();
        String merek_petikemas = txt_merek_petikemas.getText().trim();
        String jenis_kemasan = txt_jenis_kemasan.getText().trim();
        String jml_kemasan = txt_jumlah_kemasan.getText().trim();
        String merek_kemasan = txt_merek_kemasan.getText().trim();
        String tarif_hs = txt_tarif.getText().trim();
        String volume = txt_volume.getText().trim();
        String kotor = txt_kotor.getText().trim();
        String bersih = txt_bersih.getText().trim();
        String barang_id = txt_idBarang.getText().trim();
        String harga = txt_harga.getText().trim();
        String tarifBea = txt_tarifBea.getText().trim();
        String perizinan = txt_perizinan.getText().trim();
        String negara = txt_negara.getText().trim();
        String nilai_fob = txt_nilai_fob.getText().trim();
        String nilai_tukar = txt_nilai_tukar.getText().trim();
        String query = "INSERT INTO `peb_petikemas`(`petikemas`, `status_petikemas`, `jumlah_petikemas`, `merek_nomor_petikemas`, `jenis_kemasan`,"
                + " `jumlah_kemasan`, `merek_kemasan`, `tarif_hs`, `volume`, `berat_kotor`, `berat_bersih`, `barang_id`, `harga_ekspor`, "
                + "`tarif_bea`, `perizinan_ekspor`, `negara_asal_barang`, `jumlah_nilai_fob`, `nilai_tukar_mata_uang`, `transaksi_id`) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, petikemas);
            ps.setString(2, status_petikemas);
            ps.setString(3, jml_petikemas);
            ps.setString(4, merek_petikemas);
            ps.setString(5, jenis_kemasan);
            ps.setString(6, jml_kemasan);
            ps.setString(7, merek_kemasan);
            ps.setString(8, tarif_hs);
            ps.setString(9, volume);
            ps.setString(10, kotor);
            ps.setString(11, bersih);
            ps.setString(12, barang_id);
            ps.setString(13, harga);
            ps.setString(14, tarifBea);
            ps.setString(15, perizinan);
            ps.setString(16, negara);
            ps.setString(17, nilai_fob);
            ps.setString(18, nilai_tukar);
            ps.setString(19, id_Transaksi);
            ps.executeUpdate();
        } catch (Exception e) {
            showMessage("" + e);
        }
    }

    private void kosong() {
        txt_kode.setText("");
        txt_nomorPeng.setText("");
//        jComboJenis.setSelectedItem(null);
//        jComboKategori.setSelectedItem(null);
//        jComboPembayaran.setSelectedItem(null);
        txt_idEksportir.setText("");
        txt_nama.setText("");
        txt_alamat.setText("");
        txt_idPPJK.setText("");
        jRadioButton3.setSelected(true);
//        jComboPengangkutan.setSelectedItem(null);
        txt_sarana.setText("");
        txt_pengangkut.setText("");
        txt_bendera.setText("");
        jDatePerkiraan.setDate(null);
        txt_muat_asal.setText("");
        txt_muat_ekspor.setText("");
        txt_transit.setText("");
        txt_bongkar.setText("");
        txt_lokasi.setText("");
        txt_kantor.setText("");
        txt_no_invoice.setText("");
        jDateInvoice.setDate(null);
        txt_jenis_dokumen.setText("");
        txt_no_dokumen.setText("");
        jDateDokumen.setDate(null);
        txt_daerah.setText("");
        txt_tujuan.setText("");
//        jComboPenyerahan.setSelectedItem(null);
        txt_devisa.setText("");
        txt_valuta.setText("");
        txt_freight.setText("");
        txt_asuransi.setText("");
        txt_fob.setText("");
        txt_petikemas.setText("");
        txt_status_petikemas.setText("");
        txt_jumlah_petikemas.setText("");
        txt_merek_petikemas.setText("");
        txt_jenis_kemasan.setText("");
        txt_jumlah_kemasan.setText("");
        txt_merek_kemasan.setText("");
        txt_tarif.setText("");
        txt_volume.setText("");
        txt_kotor.setText("");
        txt_bersih.setText("");
        txt_idBarang.setText("");
        txt_harga.setText("");
        txt_tarifBea.setText("");
        txt_perizinan.setText("");
        txt_negara.setText("");
        txt_nilai_fob.setText("");
        txt_nilai_tukar.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabelTitle = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt_nomorPeng = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jComboJenis = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        jComboKategori = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabel13 = new javax.swing.JLabel();
        jComboPembayaran = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        txt_nama = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txt_alamat = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        txt_idPPJK = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        txt_sarana = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txt_pengangkut = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txt_bendera = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        jDatePerkiraan = new com.toedter.calendar.JDateChooser();
        jComboPengangkutan = new javax.swing.JComboBox<>();
        txt_kode = new javax.swing.JTextField();
        txt_idEksportir = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel23 = new javax.swing.JLabel();
        txt_muat_asal = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        txt_muat_ekspor = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        txt_transit = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        txt_bongkar = new javax.swing.JTextField();
        txt_no_invoice = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jDateInvoice = new com.toedter.calendar.JDateChooser();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txt_lokasi = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        txt_kantor = new javax.swing.JTextField();
        txt_jenis_dokumen = new javax.swing.JTextField();
        txt_no_dokumen = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        txt_tujuan = new javax.swing.JTextField();
        jLabel59 = new javax.swing.JLabel();
        jComboPenyerahan = new javax.swing.JComboBox<>();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        txt_devisa = new javax.swing.JTextField();
        txt_valuta = new javax.swing.JTextField();
        txt_freight = new javax.swing.JTextField();
        txt_asuransi = new javax.swing.JTextField();
        txt_daerah = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        jDateDokumen = new com.toedter.calendar.JDateChooser();
        txt_fob = new javax.swing.JTextField();
        txt_petikemas = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel64 = new javax.swing.JLabel();
        txt_status_petikemas = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        txt_jumlah_petikemas = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        txt_merek_petikemas = new javax.swing.JTextField();
        jLabel72 = new javax.swing.JLabel();
        jLabel73 = new javax.swing.JLabel();
        txt_jumlah_kemasan = new javax.swing.JTextField();
        jLabel74 = new javax.swing.JLabel();
        txt_jenis_kemasan = new javax.swing.JTextField();
        txt_merek_kemasan = new javax.swing.JTextField();
        jLabel75 = new javax.swing.JLabel();
        txt_tarif = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel76 = new javax.swing.JLabel();
        txt_volume = new javax.swing.JTextField();
        jLabel77 = new javax.swing.JLabel();
        txt_kotor = new javax.swing.JTextField();
        jLabel78 = new javax.swing.JLabel();
        txt_bersih = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txt_idBarang = new javax.swing.JTextField();
        txt_harga = new javax.swing.JTextField();
        txt_tarifBea = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txt_perizinan = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_negara = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txt_nilai_fob = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txt_nilai_tukar = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabelTitle.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabelTitle.setText("Transaksi Ekspor");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Kode Kantor Pabean");

        jLabel10.setText("Nomor Pengajuan");

        jButton1.setText("Search");
        jButton1.setPreferredSize(new java.awt.Dimension(65, 20));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel11.setText("Jenis Ekspor");

        jComboJenis.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Biasa", "Akan diimpor kembali", "Reekspor" }));

        jLabel12.setText("Kategori Ekspor");

        jComboKategori.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Umum", "KITE dengan Pembebasan", "KITE dengan Pengembalian", "KITE dengan Pembebasan dan Pengembalian", "Khusus Barang Kiriman", "Khusus Barang Pindahan", "Khusus Badan Internasional", "Khusus Barang Cinderamata", "Khusus Barang Contoh", "Khusus Barang Keperluan Penelitian", "TPB dari Kawasan Berikat", "TPB dari Gudang Berikat", "TPB dari Tempat Pameran Berikat", "TPB dari Toko Bebas BEA", "TPB dari Tempat Lelang Berikat", "TPB dari Kawasan Daur Ulang Berikat" }));

        jLabel14.setText("Cara Perdagangan");

        jRadioButton3.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(jRadioButton3);
        jRadioButton3.setText("Imbal Dagang");

        jRadioButton2.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Lainnya");

        jLabel13.setText("Cara Pembayaran");

        jComboPembayaran.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pembayaran Dimuka", "Pembayaran Kemudian", "Sight Letter of Credit", "Usance Letter of Credit", "Red Clause Letter of Credit", "Wesel Inkaso", "Konsinyasi", "Interoffice Account", "Lainnya" }));

        jLabel3.setText("ID Eksportir");

        jButton2.setText("Search");
        jButton2.setPreferredSize(new java.awt.Dimension(65, 20));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel25.setText("Nama Penerima");

        jLabel26.setText("Alamat Penerima");

        jLabel27.setText("ID PPJK");

        jLabel28.setText("Cara Pengangkutan");

        jLabel29.setText("Nama Sarana Pengangkut");

        jLabel30.setText("No. Pengangkut");

        jLabel31.setText("Bendera Sarana Pengangkut");

        jLabel32.setText("Tanggal Perkiraan Ekspor");

        jDatePerkiraan.setDateFormatString("yyyy-MM-dd");

        jComboPengangkutan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Angkutan Laut", "Kereta Api", "Angkutan Jalan Raya", "Angkutan Udara", "Jasa POS", "Angkutan Multimoda", "Instalasi / Pipa", "Angkutan Sungai", "Lainnya" }));
        jComboPengangkutan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboPengangkutanActionPerformed(evt);
            }
        });

        jButton3.setText("Search");
        jButton3.setPreferredSize(new java.awt.Dimension(65, 20));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel23.setText("Pelabuhan Muat Asal");

        jLabel33.setText("Tempat Muat Ekspor");

        jLabel34.setText("Pelabuhan Transit LN");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel33, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboPengangkutan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_sarana)
                    .addComponent(txt_pengangkut)
                    .addComponent(txt_bendera)
                    .addComponent(jDatePerkiraan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_alamat)
                    .addComponent(txt_nama)
                    .addComponent(jComboPembayaran, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jRadioButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jRadioButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jComboKategori, 0, 1, Short.MAX_VALUE)
                    .addComponent(jComboJenis, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_nomorPeng)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txt_idEksportir, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(txt_idPPJK, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(txt_kode, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(txt_muat_asal)
                    .addComponent(txt_muat_ekspor)
                    .addComponent(txt_transit))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_kode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txt_nomorPeng, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jComboJenis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jComboKategori, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jRadioButton3)
                    .addComponent(jRadioButton2))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jComboPembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_idEksportir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(txt_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(txt_alamat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(txt_idPPJK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(jComboPengangkutan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(txt_sarana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel30)
                    .addComponent(txt_pengangkut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(txt_bendera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel32)
                    .addComponent(jDatePerkiraan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(txt_muat_asal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(txt_muat_ekspor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(txt_transit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel35.setText("Pelabuhan Bongkar");

        jLabel37.setText("Tanggal Invoice");

        jDateInvoice.setDateFormatString("yyyy-MM-dd");

        jLabel38.setText("Jenis Dokumen");

        jLabel39.setText("Nomor Dokumen");

        jLabel40.setText("Tanggal Dokumen");

        jLabel41.setText("Lokasi Pemeriksaan");

        jLabel42.setText("Kantor Pabean Pemeriksaan");

        jLabel57.setText("Daerah Asal Barang");

        jLabel58.setText("Negara Tujuan Ekspor");

        jLabel59.setText("Cara Penyerahan Barang");

        jComboPenyerahan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ex Works (EXW)", "Free Carrier (FCA)", "Free Alongside Ship (FAS)", "Free on Board (FOB)", "Cost and Freight (CFR)", "Cost, Insurance, and Freight (CIF)", "Carriage Paid To (CPT)", "Carriage and Insurance Paid To (CIP)", "Delivered At Frontier (DAF)", "Delivered Ex Ship (DES)", "Delivered Ex Quay (DEQ)", "Delivered Duty Unpaid (DDU)", "Delivered Duty Paid (DDP)" }));

        jLabel49.setText("Bank Devisa Hasil Ekspor");

        jLabel50.setText("Jenis Valuta Asing");

        jLabel51.setText("Freight");

        jLabel60.setText("Asuransi (LN/DN)");

        jLabel61.setText("FOB");

        jLabel43.setText("Nomor Invoice");

        jDateDokumen.setDateFormatString("yyyy-MM-dd");

        jLabel1.setText("Peti Kemas");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel61, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel60, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel49, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel59, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel58, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel57, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel40, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel39, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel38, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel37, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel43, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel41, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txt_asuransi, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_freight, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_valuta, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_devisa, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboPenyerahan, javax.swing.GroupLayout.Alignment.LEADING, 0, 1, Short.MAX_VALUE)
                    .addComponent(txt_tujuan, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_daerah, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateDokumen, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                    .addComponent(txt_no_dokumen, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_jenis_dokumen, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateInvoice, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_no_invoice, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_kantor, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_lokasi, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_bongkar, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_fob)
                    .addComponent(txt_petikemas))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(txt_bongkar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel41)
                    .addComponent(txt_lokasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(txt_kantor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel43)
                    .addComponent(txt_no_invoice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel37)
                    .addComponent(jDateInvoice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(txt_jenis_dokumen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(txt_no_dokumen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel40)
                    .addComponent(jDateDokumen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel57)
                    .addComponent(txt_daerah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel58)
                    .addComponent(txt_tujuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel59)
                    .addComponent(jComboPenyerahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel49)
                    .addComponent(txt_devisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel50)
                    .addComponent(txt_valuta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel51)
                    .addComponent(txt_freight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel60)
                    .addComponent(txt_asuransi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel61)
                    .addComponent(txt_fob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_petikemas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel64.setText("Status Peti Kemas");

        jLabel65.setText("Jumlah Peti Kemas");

        jLabel66.setText("Merek dan Nomor Peti Kemas");

        jLabel72.setText("Jenis Kemasan");

        jLabel73.setText("Jumlah Kemasan");

        jLabel74.setText("Merek Kemasan");

        jLabel75.setText("Pos Tarif/HS");

        jLabel9.setText("ID Barang");

        jLabel76.setText("Volume");

        jLabel77.setText("Berat Kotor (kg)");

        jLabel78.setText("Berat Bersih (kg)");

        jLabel15.setText("Harga Ekspor Barang");

        jLabel16.setText("Tarif BEA Keluar");

        jLabel17.setText("Perizinan Ekspor");

        jLabel18.setText("Negara Asal Barang");

        jLabel19.setText("Jumlah Nilai FOB");

        jLabel20.setText("Nilai Tukar Mata Uang");

        jButton4.setText("Search");
        jButton4.setPreferredSize(new java.awt.Dimension(65, 20));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel78, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel77, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel76, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel75, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel74, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel73, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel72, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel66, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel65, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel64, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_jumlah_petikemas, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_status_petikemas, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_merek_petikemas)
                    .addComponent(txt_jenis_kemasan, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txt_jumlah_kemasan)
                    .addComponent(txt_merek_kemasan)
                    .addComponent(txt_tarif)
                    .addComponent(txt_volume)
                    .addComponent(txt_kotor)
                    .addComponent(txt_bersih)
                    .addComponent(txt_harga)
                    .addComponent(txt_tarifBea)
                    .addComponent(txt_perizinan)
                    .addComponent(txt_negara)
                    .addComponent(txt_nilai_fob)
                    .addComponent(txt_nilai_tukar)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(txt_idBarang, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(10, 10, 10))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel64)
                    .addComponent(txt_status_petikemas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel65)
                    .addComponent(txt_jumlah_petikemas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel66)
                    .addComponent(txt_merek_petikemas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel72)
                    .addComponent(txt_jenis_kemasan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel73)
                    .addComponent(txt_jumlah_kemasan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel74)
                    .addComponent(txt_merek_kemasan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel75)
                    .addComponent(txt_tarif, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel76)
                    .addComponent(txt_volume, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel77)
                    .addComponent(txt_kotor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel78)
                    .addComponent(txt_bersih, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txt_idBarang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txt_harga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_tarifBea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txt_perizinan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txt_negara, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txt_nilai_fob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txt_nilai_tukar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_save.setBackground(new java.awt.Color(0, 102, 204));
        btn_save.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_save.setForeground(new java.awt.Color(255, 255, 255));
        btn_save.setText("Save");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        btn_cancel.setBackground(new java.awt.Color(255, 102, 102));
        btn_cancel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cancel.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTitle)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_cancel)
                .addGap(10, 10, 10)
                .addComponent(btn_save)
                .addGap(16, 16, 16))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        TableKantorPabean pro = new TableKantorPabean();
        pro.ekspor = this;
        pro.pack();
        pro.jLabel1.setText("Data Kantor Pabean Ekspor");
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        TableDataEksportir pro = new TableDataEksportir();
        pro.ekspor = this;
        pro.pack();
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboPengangkutanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboPengangkutanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboPengangkutanActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        TableDataPPJK pro = new TableDataPPJK();
        pro.ekspor = this;
        pro.pack();
        pro.jLabelTitle.setText("Data PPJK Ekspor");
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        TableDataBarang pro = new TableDataBarang();
        pro.ekspor = this;
        pro.pack();
        pro.jLabelTitle.setText("Data Barang Ekspor");
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        // TODO add your handling code here:
        String kode = txt_kode.getText().trim();
        String pengajuan = txt_nomorPeng.getText().trim();
        jenis = jComboJenis.getSelectedItem().toString();
        kategori = jComboKategori.getSelectedItem().toString();
        pembayaran = jComboPembayaran.getSelectedItem().toString();
        String eksportir = txt_idEksportir.getText().trim();
        String nama = txt_nama.getText().trim();
        String alamat = txt_alamat.getText().trim();
        String ppjk = txt_idPPJK.getText().trim();
        pengangkutan = jComboPengangkutan.getSelectedItem().toString();
        String sarana = txt_sarana.getText().trim();
        String no_pengangkut = txt_pengangkut.getText().trim();
        String bendera = txt_bendera.getText().trim();
        perkiraan = jDatePerkiraan.getDate();
        String asal = txt_muat_asal.getText().trim();
        String tempat_muat = txt_muat_ekspor.getText().trim();
        String transit = txt_transit.getText().trim();
        String bongkar = txt_bongkar.getText().trim();
        String lokasi = txt_lokasi.getText().trim();
        String kantor = txt_kantor.getText().trim();
        String invoice = txt_no_invoice.getText().trim();
        t_invoice = jDateInvoice.getDate();
        String jenis_dokumen = txt_jenis_dokumen.getText().trim();
        String no_dokumen = txt_no_dokumen.getText().trim();
        t_dokumen = jDateDokumen.getDate();
        String daerah = txt_daerah.getText().trim();
        String tujuan = txt_tujuan.getText().trim();
        penyerahan = jComboPenyerahan.getSelectedItem().toString();
        String devisa = txt_devisa.getText().trim();
        String valuta = txt_valuta.getText().trim();
        String freight = txt_freight.getText().trim();
        String asuransi = txt_asuransi.getText().trim();
        String fob = txt_fob.getText().trim();
        String petikemas = txt_petikemas.getText().trim();
        String status_petikemas = txt_status_petikemas.getText().trim();
        String jml_petikemas = txt_jumlah_petikemas.getText().trim();
        String merek_petikemas = txt_merek_petikemas.getText().trim();
        String jenis_kemasan = txt_jenis_kemasan.getText().trim();
        String jml_kemasan = txt_jumlah_kemasan.getText().trim();
        String merek_kemasan = txt_merek_kemasan.getText().trim();
        String tarif_hs = txt_tarif.getText().trim();
        String volume = txt_volume.getText().trim();
        String kotor = txt_kotor.getText().trim();
        String bersih = txt_bersih.getText().trim();
        String barang_id = txt_idBarang.getText().trim();
        String harga = txt_harga.getText().trim();
        String tarifBea = txt_tarifBea.getText().trim();
        String perizinan = txt_perizinan.getText().trim();
        String negara = txt_negara.getText().trim();
        String nilai_fob = txt_nilai_fob.getText().trim();
        String nilai_tukar = txt_nilai_tukar.getText().trim();
        if (kode.isEmpty()) {
            showMessage("Kode cannot be empty!");
            txt_kode.requestFocus();
        } else if (pengajuan.isEmpty()) {
            showMessage("Nomor cannot be empty!");
            txt_nomorPeng.requestFocus();
        } else if (jenis == null) {
            showMessage("Jenis ekspor cannot be empty!");
        } else if (kategori == null) {
            showMessage("Kategori ekspor cannot be empty!");
        } else if (pembayaran == null) {
            showMessage("Cara pembayaran cannot be empty!");
        } else if (eksportir.isEmpty()) {
            showMessage("ID eksportir cannot be empty!");
            txt_idEksportir.requestFocus();
        } else if (nama.isEmpty()) {
            showMessage("Nama penerima cannot be empty!");
            txt_nama.requestFocus();
        } else if (alamat.isEmpty()) {
            showMessage("Alamat penrima cannot be empty!");
            txt_alamat.requestFocus();
        } else if (ppjk.isEmpty()) {
            showMessage("ID PPJK cannot be empty!");
            txt_idPPJK.requestFocus();
        } else if (pengangkutan == null) {
            showMessage("Cara pengangkutan cannot be empty!");
        } else if (sarana.isEmpty()) {
            showMessage("Nama sarana pengangkut cannot be empty!");
            txt_sarana.requestFocus();
        } else if (no_pengangkut.isEmpty()) {
            showMessage("Nomor pengangkut cannot be empty!");
            txt_pengangkut.requestFocus();
        } else if (bendera.isEmpty()) {
            showMessage("Bendera sarana pengangkut cannot be empty!");
            txt_bendera.requestFocus();
        } else if (perkiraan == null) {
            showMessage("Tanggal perikraan ekspor cannot be empty!");
            jDatePerkiraan.requestFocus();
        } else if (asal.isEmpty()) {
            showMessage("Pelabuhan muat asal cannot be empty!");
            txt_muat_asal.requestFocus();
        } else if (tempat_muat.isEmpty()) {
            showMessage("Tempat muat ekspor cannot be empty!");
            txt_muat_ekspor.requestFocus();
        } else if (transit.isEmpty()) {
            showMessage("Pelabuhan transit cannot be empty!");
            txt_transit.requestFocus();
        } else if (bongkar.isEmpty()) {
            showMessage("Pelabuhan bongkar cannot be empty!");
            txt_bongkar.requestFocus();
        } else if (lokasi.isEmpty()) {
            showMessage("Lokasi Pemeriksaan cannot be empty!");
            txt_lokasi.requestFocus();
        } else if (kantor.isEmpty()) {
            showMessage("Kantor Pabean Pemeriksaan cannot be empty!");
            txt_kantor.requestFocus();
        } else if (invoice.isEmpty()) {
            showMessage("Nomor Invoice cannot be empty!");
            txt_no_invoice.requestFocus();
        } else if (t_invoice == null) {
            showMessage("Tanggal Invoice cannot be empty!");
            jDateInvoice.requestFocus();
        } else if (jenis_dokumen.isEmpty()) {
            showMessage("Jenis Dokumen cannot be empty!");
            txt_jenis_dokumen.requestFocus();
        } else if (no_dokumen.isEmpty()) {
            showMessage("Nomor Dokumen cannot be empty!");
            txt_no_dokumen.requestFocus();
        } else if (t_dokumen == null) {
            showMessage("Tanggal Dokumen cannot be empty!");
            jDateDokumen.requestFocus();
        } else if (daerah.isEmpty()) {
            showMessage("Daerah Asal Barang cannot be empty!");
            txt_daerah.requestFocus();
        } else if (tujuan.isEmpty()) {
            showMessage("Negara Tujuan Ekspor cannot be empty!");
            txt_tujuan.requestFocus();
        } else if (penyerahan == null) {
            showMessage("Cara Penyerahan Barang cannot be empty!");
            jComboPenyerahan.requestFocus();
        } else if (devisa.isEmpty()) {
            showMessage("Bank Devisa Hasil Ekspor cannot be empty!");
            txt_devisa.requestFocus();
        } else if (valuta.isEmpty()) {
            showMessage("Jenis Valuta Asing cannot be empty!");
            txt_valuta.requestFocus();
        } else if (freight.isEmpty()) {
            showMessage("Freight cannot be empty!");
            txt_freight.requestFocus();
        } else if (asuransi.isEmpty()) {
            showMessage("Asuransi (LN/DN) cannot be empty!");
            txt_asuransi.requestFocus();
        } else if (fob.isEmpty()) {
            showMessage("FOB cannot be empty!");
            txt_fob.requestFocus();
        } else if (petikemas.isEmpty()) {
            showMessage("Peti Kemas cannot be empty!");
            txt_petikemas.requestFocus();
        } else if (status_petikemas.isEmpty()) {
            showMessage("Status Peti Kemas cannot be empty!");
            txt_status_petikemas.requestFocus();
        } else if (jml_petikemas.isEmpty()) {
            showMessage("Jumlah Peti Kemas cannot be empty!");
            txt_jumlah_petikemas.requestFocus();
        } else if (merek_petikemas.isEmpty()) {
            showMessage("Merek dan Nomor Peti Kemas cannot be empty!");
            txt_merek_petikemas.requestFocus();
        } else if (jenis_kemasan.isEmpty()) {
            showMessage("Jenis Kemasan cannot be empty!");
            txt_jenis_kemasan.requestFocus();
        } else if (jml_kemasan.isEmpty()) {
            showMessage("Jumlah Kemasan cannot be empty!");
            txt_jumlah_kemasan.requestFocus();
        } else if (merek_kemasan.isEmpty()) {
            showMessage("Merek Kemasan cannot be empty!");
            txt_merek_kemasan.requestFocus();
        } else if (tarif_hs.isEmpty()) {
            showMessage("Pos Tarif/HS cannot be empty!");
            txt_tarif.requestFocus();
        } else if (volume.isEmpty()) {
            showMessage("Volume cannot be empty!");
            txt_volume.requestFocus();
        } else if (kotor.isEmpty()) {
            showMessage("Berat Kotor (kg) cannot be empty!");
            txt_kotor.requestFocus();
        } else if (bersih.isEmpty()) {
            showMessage("Berat Bersih (kg) cannot be empty!");
            txt_bersih.requestFocus();
        } else if (barang_id.isEmpty()) {
            showMessage("ID Barang cannot be empty!");
            txt_idBarang.requestFocus();
        } else if (harga.isEmpty()) {
            showMessage("Harga Ekspor Barang cannot be empty!");
            txt_harga.requestFocus();
        } else if (tarifBea.isEmpty()) {
            showMessage("Tarif BEA Keluar cannot be empty!");
            txt_tarifBea.requestFocus();
        } else if (perizinan.isEmpty()) {
            showMessage("Perizinan Ekspor cannot be empty!");
            txt_perizinan.requestFocus();
        } else if (negara.isEmpty()) {
            showMessage("Negara Asal Barang cannot be empty!");
            txt_negara.requestFocus();
        } else if (nilai_fob.isEmpty()) {
            showMessage("Jumlah Nilai FOB cannot be empty!");
            txt_nilai_fob.requestFocus();
        } else if (nilai_tukar.isEmpty()) {
            showMessage("Nilai Tukar Mata Uang cannot be empty!");
            txt_nilai_tukar.requestFocus();
        } else {
            saveDataPEB();
            saveDataPEBPelabuhan();
            saveDataPEBPetikemas();
            showMessage("Data saved successfully!");
            printData();
            kosong();
            autonumber();
        }
    }//GEN-LAST:event_btn_saveActionPerformed
    private void printData() {
        try {
            String namaFile = "src\\report\\reportTransaksiEkspor.jasper";
            HashMap param = new HashMap();
            param.put("transaksi", id_Transaksi);
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        // TODO add your handling code here:
        kosong();
    }//GEN-LAST:event_btn_cancelActionPerformed
    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_save;
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox<String> jComboJenis;
    private javax.swing.JComboBox<String> jComboKategori;
    private javax.swing.JComboBox<String> jComboPembayaran;
    private javax.swing.JComboBox<String> jComboPengangkutan;
    private javax.swing.JComboBox<String> jComboPenyerahan;
    private com.toedter.calendar.JDateChooser jDateDokumen;
    private com.toedter.calendar.JDateChooser jDateInvoice;
    private com.toedter.calendar.JDateChooser jDatePerkiraan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JLabel jLabelTitle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txt_alamat;
    private javax.swing.JTextField txt_asuransi;
    private javax.swing.JTextField txt_bendera;
    private javax.swing.JTextField txt_bersih;
    private javax.swing.JTextField txt_bongkar;
    private javax.swing.JTextField txt_daerah;
    private javax.swing.JTextField txt_devisa;
    private javax.swing.JTextField txt_fob;
    private javax.swing.JTextField txt_freight;
    private javax.swing.JTextField txt_harga;
    private javax.swing.JTextField txt_idBarang;
    private javax.swing.JTextField txt_idEksportir;
    private javax.swing.JTextField txt_idPPJK;
    private javax.swing.JTextField txt_jenis_dokumen;
    private javax.swing.JTextField txt_jenis_kemasan;
    private javax.swing.JTextField txt_jumlah_kemasan;
    private javax.swing.JTextField txt_jumlah_petikemas;
    private javax.swing.JTextField txt_kantor;
    private javax.swing.JTextField txt_kode;
    private javax.swing.JTextField txt_kotor;
    private javax.swing.JTextField txt_lokasi;
    private javax.swing.JTextField txt_merek_kemasan;
    private javax.swing.JTextField txt_merek_petikemas;
    private javax.swing.JTextField txt_muat_asal;
    private javax.swing.JTextField txt_muat_ekspor;
    private javax.swing.JTextField txt_nama;
    private javax.swing.JTextField txt_negara;
    private javax.swing.JTextField txt_nilai_fob;
    private javax.swing.JTextField txt_nilai_tukar;
    private javax.swing.JTextField txt_no_dokumen;
    private javax.swing.JTextField txt_no_invoice;
    private javax.swing.JTextField txt_nomorPeng;
    private javax.swing.JTextField txt_pengangkut;
    private javax.swing.JTextField txt_perizinan;
    private javax.swing.JTextField txt_petikemas;
    private javax.swing.JTextField txt_sarana;
    private javax.swing.JTextField txt_status_petikemas;
    private javax.swing.JTextField txt_tarif;
    private javax.swing.JTextField txt_tarifBea;
    private javax.swing.JTextField txt_transit;
    private javax.swing.JTextField txt_tujuan;
    private javax.swing.JTextField txt_valuta;
    private javax.swing.JTextField txt_volume;
    // End of variables declaration//GEN-END:variables
}
