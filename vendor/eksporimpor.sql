-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Jul 2020 pada 07.08
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eksporimpor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id` varchar(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `jenis` varchar(128) NOT NULL,
  `jumlah` varchar(11) NOT NULL,
  `merek` varchar(128) NOT NULL,
  `tipe` varchar(128) NOT NULL,
  `ukuran` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id`, `nama`, `jenis`, `jumlah`, `merek`, `tipe`, `ukuran`) VALUES
('IBR00000001', 'Mesin penyetrika (Ironing Machines)', '250 watt', '1000', 'Merek Sonya', 'SNA-250', '40 X 40 cm');

-- --------------------------------------------------------

--
-- Struktur dari tabel `eksportir`
--

CREATE TABLE `eksportir` (
  `id` varchar(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `npwp` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `niper` varchar(128) NOT NULL,
  `status` varchar(50) NOT NULL,
  `nomor_tdp` varchar(128) NOT NULL,
  `tgl_tdp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `eksportir`
--

INSERT INTO `eksportir` (`id`, `nama`, `npwp`, `alamat`, `niper`, `status`, `nomor_tdp`, `tgl_tdp`) VALUES
('IEK00000001', 'Sumber Makmur, PT.', '01.061.747.0-092.000', 'Jalan Mandiri 77, Cakung, Jakarta Utara.', '-----', 'PMA (non migas)', '123732', '2020-07-01'),
('IEK00000002', 'Maju Mulya, PT.', '01.011.990.0-801.210', 'Jl. Cut Mutia No.48, Gulak Galik, Kec. Tlk. Betung Utara, Kota Bandar Lampung, Lampung 35214', '----', 'Koperasi', '-----', '2020-07-09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `importir`
--

CREATE TABLE `importir` (
  `id` varchar(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `npwp` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `status` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `importir`
--

INSERT INTO `importir` (`id`, `nama`, `npwp`, `alamat`, `status`, `created_at`) VALUES
('IMP00000001', 'Sumber Makmur, PT.', '01.061.747.0-092.000', 'Jalan Samudera 1 No. 15, Jakarta Utara', 'Importir Umum', '2020-06-29 13:39:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoice_pib`
--

CREATE TABLE `invoice_pib` (
  `id` int(11) NOT NULL,
  `nomor_invoice` varchar(128) NOT NULL,
  `tanggal_invoice` varchar(128) NOT NULL,
  `nomor_lc` varchar(128) NOT NULL,
  `tanggal_lc` varchar(128) NOT NULL,
  `nomor_awb` varchar(128) NOT NULL,
  `tanggal_awb` varchar(128) NOT NULL,
  `nomor_fasilitas` varchar(128) NOT NULL,
  `tanggal_fasilitas` varchar(128) NOT NULL,
  `tempat_penimbunan` varchar(128) NOT NULL,
  `valuta` varchar(128) NOT NULL,
  `ndpbm` varchar(128) NOT NULL,
  `fob` varchar(128) NOT NULL,
  `freight` varchar(128) NOT NULL,
  `asuransi` varchar(128) NOT NULL,
  `nilai_cif` varchar(10) NOT NULL,
  `nomor_petikemas` varchar(10) NOT NULL,
  `ukuran_petikemas` varchar(10) NOT NULL,
  `tipe_petikemas` varchar(128) NOT NULL,
  `transaksi_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `invoice_pib`
--

INSERT INTO `invoice_pib` (`id`, `nomor_invoice`, `tanggal_invoice`, `nomor_lc`, `tanggal_lc`, `nomor_awb`, `tanggal_awb`, `nomor_fasilitas`, `tanggal_fasilitas`, `tempat_penimbunan`, `valuta`, `ndpbm`, `fob`, `freight`, `asuransi`, `nilai_cif`, `nomor_petikemas`, `ukuran_petikemas`, `tipe_petikemas`, `transaksi_id`) VALUES
(1, '23842930', '2020-06-16', '238942937', '2020-06-16', '33892473298', '2020-06-11', '08320823', '2020-06-12', 'dgdfg', 'dfdd', 'dsgsdf', 'dgfdgd', '8', '9o', '8', 'jf', 'ksdfds', 'dsfgsd', 'IDT00000002'),
(2, '1232431', '2020-06-30', '09877363', '2020-06-30', '098398323', '2020-06-30', '09234932', '2020-06-30', 'KPU Jakarta', 'United State Dollar', '50000', '50000', '1000', '(LN) 250', '9700000', '7234723', '23', 'Abstrak', 'IDT00000003'),
(3, '1232431', '2020-06-30', '09877363', '2020-06-30', '098398323', '2020-06-30', '09234932', '2020-06-30', 'KPU Jakarta', 'United State Dollar', '50000', '50000', '1000', '(LN) 250', '9700000', '7234723', '23', 'Abstrak', 'IDT00000004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kantor_pabean`
--

CREATE TABLE `kantor_pabean` (
  `id` int(11) NOT NULL,
  `kode_kantor` int(10) NOT NULL,
  `nama_kantor` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kantor_pabean`
--

INSERT INTO `kantor_pabean` (`id`, `kode_kantor`, `nama_kantor`, `alamat`, `status`, `created_at`) VALUES
(1, 10000, 'Kantor Wilayah DJBC Sumatera Utara', 'Jl. Pangeran Diponegoro No.30 A, Madras Hulu, Medan Polonia, Medan City, North Sumatra 20152', 'Aktif', '2020-07-03 18:24:29'),
(3, 20000, 'Kantor Wilayah DJBC Khusus Kepulauan Riau', 'Jl. Jend. A. Yani, Tanjung Balai Riau, Meral, Meral Kota, Kec. Karimun, Kabupaten Karimun, Kepulauan Riau 29664', 'Aktif', '2020-07-03 18:24:29'),
(4, 20400, 'KPU BC Tipe B Batam', 'Jl. Kuda Laut, Sungai Jodoh, Kec. Batu Ampar, Kota Batam, Kepulauan Riau 29432', 'Aktif', '2020-07-03 18:24:29'),
(5, 40300, 'KPU BC Tipe A Tanjung Priok', 'Pabean No.1, Tj. Priok, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14310', 'Aktif', '2020-07-03 18:20:17'),
(6, 50100, 'KPU BC Tipe C Soekarno – Hatta', 'Jl. Raya Bandara, Cengkareng, Pajang, Benda, RT.001/RW.010, Pajang, Benda, Kota Tangerang, Banten 15126', 'Aktif', '2020-07-03 19:10:08'),
(7, 30000, 'Kantor Wilayah DJBC Sumatera Bagian Timur', 'Jl. Cut Mutia No.48, Gulak Galik, Kec. Tlk. Betung Utara, Kota Bandar Lampung, Lampung 35214', 'Aktif', '2020-07-03 18:26:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kemasan_pib`
--

CREATE TABLE `kemasan_pib` (
  `id` int(11) NOT NULL,
  `jumlah` varchar(10) NOT NULL,
  `jenis` varchar(128) NOT NULL,
  `merek` varchar(128) NOT NULL,
  `berat_kotor` varchar(10) NOT NULL,
  `berat_bersih` varchar(10) NOT NULL,
  `tarif_hs` varchar(10) NOT NULL,
  `id_barang` varchar(11) NOT NULL,
  `jenis_fasilitas` varchar(128) NOT NULL,
  `negara_asal` varchar(128) NOT NULL,
  `jumlah_barang` varchar(10) NOT NULL,
  `jenis_satuan_barang` varchar(10) NOT NULL,
  `jumlah_nilai_cif` varchar(10) NOT NULL,
  `pembayaran` varchar(50) NOT NULL,
  `jaminan` varchar(128) NOT NULL,
  `transaksi_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kemasan_pib`
--

INSERT INTO `kemasan_pib` (`id`, `jumlah`, `jenis`, `merek`, `berat_kotor`, `berat_bersih`, `tarif_hs`, `id_barang`, `jenis_fasilitas`, `negara_asal`, `jumlah_barang`, `jenis_satuan_barang`, `jumlah_nilai_cif`, `pembayaran`, `jaminan`, `transaksi_id`) VALUES
(1, 'fsgd', '3498', 'kjdsf', '84', '39', 'we', 'IBR00000001', '34', 'dfgdf', 'dsgfd', 'dfgf', 'sdf', 'Bank', 'Tunai', 'IDT00000002'),
(2, '100', 'Package', '-', '10', '8', '9000000', 'IBR00000001', 'CEPT', 'Korea Selatan', '3', 'PCS', '21', 'Kantor Pabean', 'Customs Bond', 'IDT00000004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peb`
--

CREATE TABLE `peb` (
  `id_transaksi` varchar(11) NOT NULL,
  `kantor_id` varchar(20) NOT NULL,
  `nomor_pengajuan` varchar(128) NOT NULL,
  `jenis_ekspor` varchar(128) NOT NULL,
  `kategori_ekspor` varchar(128) NOT NULL,
  `cara_perdagangan` varchar(128) NOT NULL,
  `cara_pembayaran` varchar(128) NOT NULL,
  `eksportir_id` varchar(11) NOT NULL,
  `nama_penerima` varchar(128) NOT NULL,
  `alamat_penerima` text NOT NULL,
  `ppjk_id` varchar(11) NOT NULL,
  `cara_pengangkutan` varchar(128) NOT NULL,
  `nama_sarana_pengangkut` varchar(128) NOT NULL,
  `nomor_pengangkut` varchar(128) NOT NULL,
  `bendera_sarana_pengangkut` varchar(128) NOT NULL,
  `tanggal_perkiraan_ekspor` varchar(128) NOT NULL,
  `create_at` date NOT NULL DEFAULT current_timestamp(),
  `user_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `peb`
--

INSERT INTO `peb` (`id_transaksi`, `kantor_id`, `nomor_pengajuan`, `jenis_ekspor`, `kategori_ekspor`, `cara_perdagangan`, `cara_pembayaran`, `eksportir_id`, `nama_penerima`, `alamat_penerima`, `ppjk_id`, `cara_pengangkutan`, `nama_sarana_pengangkut`, `nomor_pengangkut`, `bendera_sarana_pengangkut`, `tanggal_perkiraan_ekspor`, `create_at`, `user_id`) VALUES
('IDT00000001', '50100', '10001-000001-20200701-000001', 'Biasa', 'Umum', 'Imbal Dagang', 'Pembayaran Dimuka', 'IEK00000001', 'klgjdl', 'kjlkdfgvkdfng;ldkjlfs;ldfksd\'lfksd;fksd;lfkdslfjlksdjfkjsdhfkjsdhfjshfjsdbnfkj h', 'IPP00000001', 'Angkutan Laut', 'gjhjkljl', 'kjhkjhk', 'vhjkjh', 'Fri Jul 10 00:00:00 ICT 2020', '2020-07-02', 'ID00000002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peb_pelabuhan`
--

CREATE TABLE `peb_pelabuhan` (
  `id` int(11) NOT NULL,
  `pelabuhan_muat_asal` varchar(128) NOT NULL,
  `tempat_muat_ekspor` varchar(128) NOT NULL,
  `pelabuhan_transit` varchar(128) NOT NULL,
  `plabuhan_bongkar` varchar(128) NOT NULL,
  `lokasi_pemerikasaan` varchar(128) NOT NULL,
  `kantor_pabean_pemerikasaan` varchar(128) NOT NULL,
  `nomor_invoice` varchar(128) NOT NULL,
  `tanggal_invoice` varchar(128) NOT NULL,
  `jenis_dokumen` varchar(128) NOT NULL,
  `nomor_dokumen` varchar(128) NOT NULL,
  `tanggal_dokumen` varchar(128) NOT NULL,
  `daerah_asal_barang` varchar(128) NOT NULL,
  `negara_tujuan` varchar(128) NOT NULL,
  `cara_penyerahan_barang` varchar(128) NOT NULL,
  `bank_devisa` varchar(128) NOT NULL,
  `jenis_valuta` varchar(128) NOT NULL,
  `freight` varchar(128) NOT NULL,
  `asuransi` varchar(128) NOT NULL,
  `fob` varchar(128) NOT NULL,
  `transaksi_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `peb_pelabuhan`
--

INSERT INTO `peb_pelabuhan` (`id`, `pelabuhan_muat_asal`, `tempat_muat_ekspor`, `pelabuhan_transit`, `plabuhan_bongkar`, `lokasi_pemerikasaan`, `kantor_pabean_pemerikasaan`, `nomor_invoice`, `tanggal_invoice`, `jenis_dokumen`, `nomor_dokumen`, `tanggal_dokumen`, `daerah_asal_barang`, `negara_tujuan`, `cara_penyerahan_barang`, `bank_devisa`, `jenis_valuta`, `freight`, `asuransi`, `fob`, `transaksi_id`) VALUES
(5, 'hkjjklj', 'jhkjh', 'kjhkljh', 'hkhkj', 'jhjhjlk', 'lkjlk', 'hjlk', 'Fri Jul 10 00:00:00 ICT 2020', 'lkjklj', 'lkjlkj', 'Tue Jul 21 00:00:00 ICT 2020', 'lkjkljlk', 'lkjlkjl', 'Ex Works (EXW)', 'kljlkj', 'lkjlkj', 'lkjlj', 'lkjlkj', 'jkhkj', 'IDT00000001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peb_petikemas`
--

CREATE TABLE `peb_petikemas` (
  `id` int(11) NOT NULL,
  `petikemas` varchar(128) NOT NULL,
  `status_petikemas` varchar(128) NOT NULL,
  `jumlah_petikemas` varchar(128) NOT NULL,
  `merek_nomor_petikemas` varchar(128) NOT NULL,
  `jenis_kemasan` varchar(128) NOT NULL,
  `jumlah_kemasan` varchar(20) NOT NULL,
  `merek_kemasan` varchar(128) NOT NULL,
  `tarif_hs` varchar(128) NOT NULL,
  `volume` varchar(20) NOT NULL,
  `berat_kotor` varchar(20) NOT NULL,
  `berat_bersih` varchar(20) NOT NULL,
  `barang_id` varchar(11) NOT NULL,
  `harga_ekspor` varchar(128) NOT NULL,
  `tarif_bea` varchar(128) NOT NULL,
  `perizinan_ekspor` varchar(128) NOT NULL,
  `negara_asal_barang` varchar(128) NOT NULL,
  `jumlah_nilai_fob` varchar(128) NOT NULL,
  `nilai_tukar_mata_uang` varchar(128) NOT NULL,
  `transaksi_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `peb_petikemas`
--

INSERT INTO `peb_petikemas` (`id`, `petikemas`, `status_petikemas`, `jumlah_petikemas`, `merek_nomor_petikemas`, `jenis_kemasan`, `jumlah_kemasan`, `merek_kemasan`, `tarif_hs`, `volume`, `berat_kotor`, `berat_bersih`, `barang_id`, `harga_ekspor`, `tarif_bea`, `perizinan_ekspor`, `negara_asal_barang`, `jumlah_nilai_fob`, `nilai_tukar_mata_uang`, `transaksi_id`) VALUES
(3, 'lkjkljl', 'lkjlkj', 'jhkjh', 'jkhkjh', 'jkhkjh', 'jkhkjh', 'kjkhjh', 'kjkhh', 'khhkh', 'kk', 'kjh', 'IBR00000001', 'kjhkjh', 'jdfhgdk', 'khkjh', 'hjkhkj', 'jhkjh', 'kjkjjk', 'IDT00000001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemilik_barang`
--

CREATE TABLE `pemilik_barang` (
  `id` varchar(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `npwp` varchar(128) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pemilik_barang`
--

INSERT INTO `pemilik_barang` (`id`, `nama`, `npwp`, `alamat`) VALUES
('IPB00000001', 'Abadi Jaya Industri, PT.', '01.034.453.0-094.000', 'Jalan Paus No.15, Jakarta Barat.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjual`
--

CREATE TABLE `penjual` (
  `id` varchar(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `negara` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penjual`
--

INSERT INTO `penjual` (`id`, `nama`, `alamat`, `negara`) VALUES
('IP00000001', 'Kwang Myung Non-Woven Co, Ltd', '281-8 Hakiang-Dong, Sasang-Gu, Pusan', 'South Korea');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pib`
--

CREATE TABLE `pib` (
  `id_transaksi` varchar(11) NOT NULL,
  `kode_kantor` varchar(10) NOT NULL,
  `nomor_pengajuan` varchar(128) NOT NULL,
  `jenis_pib` varchar(128) NOT NULL,
  `jenis_impor` varchar(128) NOT NULL,
  `cara_pembayaran` varchar(128) NOT NULL,
  `pemasok_id` varchar(11) NOT NULL,
  `importir_id` varchar(20) NOT NULL,
  `pemilik_barang_id` varchar(11) NOT NULL,
  `ppjk_id` varchar(11) NOT NULL,
  `cara_pengangkutan` varchar(128) NOT NULL,
  `nama_sarana` varchar(128) NOT NULL,
  `tanggal_tiba` varchar(128) NOT NULL,
  `pelabuhan_muat` varchar(128) NOT NULL,
  `pelabuhan_transit` varchar(128) NOT NULL,
  `pelabuhan_bongkar` varchar(128) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `user_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pib`
--

INSERT INTO `pib` (`id_transaksi`, `kode_kantor`, `nomor_pengajuan`, `jenis_pib`, `jenis_impor`, `cara_pembayaran`, `pemasok_id`, `importir_id`, `pemilik_barang_id`, `ppjk_id`, `cara_pengangkutan`, `nama_sarana`, `tanggal_tiba`, `pelabuhan_muat`, `pelabuhan_transit`, `pelabuhan_bongkar`, `created_at`, `user_id`) VALUES
('IDT00000002', '50100', '10001-032390-19700101-032390', 'Biasa', 'Untuk Dipakai', 'Biasa/Tunai', 'IP00000001', 'IMP00000001', 'IPB00000001', 'IPP00000001', 'Angkutan Laut', 'sdfklasd', '2020-06-17', 'ksdnflds', 'kdsjfd', 'lsdfl', '2020-07-04', 'ID00000001'),
('IDT00000004', '50100', '10001-000003-20200630-000003', 'Biasa', 'Untuk Dipakai', 'Biasa/Tunai', 'IP00000001', 'IMP00000001', 'IPB00000001', 'IPP00000001', 'Angkutan Laut', 'MV. Mandiri Jaya Voy. 102S', '2020-06-30', 'Kobe, Japan', 'Busan, Korea', 'Ujung Pandang, Indonesia', '2020-06-30', 'ID00000002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ppjk`
--

CREATE TABLE `ppjk` (
  `id` varchar(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `npwp` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `nomor` varchar(128) NOT NULL,
  `tgl_ppjk` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `ppjk`
--

INSERT INTO `ppjk` (`id`, `nama`, `npwp`, `alamat`, `nomor`, `tgl_ppjk`) VALUES
('IPP00000001', 'Pusaka Perdana Jaya Kencana, PT.', '01.323.792.0-011.000', 'Jalan Enggano No.50, Tanjung Priok, Jakarta Utara', '50', '2020-06-29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_id`
--

CREATE TABLE `role_id` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `role_id`
--

INSERT INTO `role_id` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Karyawan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `name`, `username`, `password`, `created_at`, `is_active`, `role_id`) VALUES
('ID00000001', 'Administrator', 'admin', 'admin', '2020-07-03 15:45:47', 1, 1),
('ID00000002', 'Muhamad Nur Sukur', 'nur', 'nur', '2020-07-03 15:45:37', 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `eksportir`
--
ALTER TABLE `eksportir`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `importir`
--
ALTER TABLE `importir`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `invoice_pib`
--
ALTER TABLE `invoice_pib`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kantor_pabean`
--
ALTER TABLE `kantor_pabean`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kemasan_pib`
--
ALTER TABLE `kemasan_pib`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `peb`
--
ALTER TABLE `peb`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `peb_pelabuhan`
--
ALTER TABLE `peb_pelabuhan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `peb_petikemas`
--
ALTER TABLE `peb_petikemas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pemilik_barang`
--
ALTER TABLE `pemilik_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penjual`
--
ALTER TABLE `penjual`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pib`
--
ALTER TABLE `pib`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `ppjk`
--
ALTER TABLE `ppjk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_id`
--
ALTER TABLE `role_id`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `invoice_pib`
--
ALTER TABLE `invoice_pib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `kantor_pabean`
--
ALTER TABLE `kantor_pabean`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `kemasan_pib`
--
ALTER TABLE `kemasan_pib`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `peb_pelabuhan`
--
ALTER TABLE `peb_pelabuhan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `peb_petikemas`
--
ALTER TABLE `peb_petikemas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `role_id`
--
ALTER TABLE `role_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role_id` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
